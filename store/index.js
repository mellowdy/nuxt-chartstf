import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import https from 'https'
const axiosInstance = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})
Vue.use(Vuex)

/* eslint-disable */
const createStore = () => {
  return new Vuex.Store({
    state: {
      collections: [],
      userCollections: [],
      collectionUrl: '',
      identifier: '',
      stats: []
    },
    getters: {
      // Data
      getCollections: state => state.collections,
      getStats: state => state.stats,

      // Classes
      getScouts: state => state.stats.filter(stat => stat.class == 'scout'),
      getSoldiers: state => state.stats.filter(stat => stat.class == 'soldier'),
      getPyros: state => state.stats.filter(stat => stat.class == 'pyro'),
      getDemomen: state => state.stats.filter(stat => stat.class == 'demoman'),
      getHeavies: state =>
        state.stats.filter(stat => stat.class == 'heavyweapons'),
      getEngineers: state =>
        state.stats.filter(stat => stat.class == 'engineer'),
      getMedics: state => state.stats.filter(stat => stat.class == 'medic'),
      getSnipers: state => state.stats.filter(stat => stat.class == 'sniper'),
      getSpies: state => state.stats.filter(stat => stat.class == 'spy')
    },
    mutations: {
      setCollections(state, c) {
        state.collections = c
      },
      setUserCollections(state, c) {
        state.userCollections = c
      },
      setStats(state, s) {
        state.stats = s
      }
    },
    actions: {
      async fetchCollections({ commit }) {
        const res = await axiosInstance.get(
          `https://localhost:44356/api/collections`
        )
        commit('setCollections', res.data)
      },
      async fetchStats({ commit }, shortUrl) {
        const res = await axiosInstance.get(
          `https://localhost:44356/api/collections/` + shortUrl
        )
        commit('setStats', res.data)
      }
    }
  })
}

export default createStore
