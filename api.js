let axios = require('axios')
let https = require('https')
let fs = require('fs')
const axiosInstance = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})
axiosInstance.get('https://localhost:44356/api/collections').then(response => {
  let collections = response.data
  fs.writeFile(
    'static/gallery.json',
    JSON.stringify(collections, null, 0),
    err => {
      if (err) throw err
    }
  )
  collections.forEach(function(item, index) {
    let shortUrl = item.shortUrl
    let url = axiosInstance.get(
      ('https://localhost:44356/api/collections/' + shortUrl)
    ).then(response => {
      fs.writeFile(
        `static/${shortUrl}.json`,
        JSON.stringify(response.data, null, 0),
        err => {
          if (err) throw err
        }
      )
    })
  })
})
