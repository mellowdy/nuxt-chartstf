
export default {
  mode: 'universal',
  generate: {
    routes: [
      '/gallery',
      '/collections/etf2ls24prem',
      '/collections/etf2ls23prem',
      '/collections/etf2ls22prem',
      '/collections/etf2ls21prem',
      '/collections/etf2ls20prem',
      '/collections/etf2ls19prem',
      '/collections/etf2ls18prem',
      '/collections/etf2ls17prem',
      '/collections/etf2ls16prem',
      '/collections/etf2ls15prem',
      '/collections/etf2ls14prem',
      '/collections/etf2ls13prem',
      '/collections/etf2ls12prem',
      '/collections/etf2ls11prem',
      '/collections/etf2ls10prem',
      '/collections/etf2ls09prem',
      '/collections/etf2ls08prem',
      '/collections/etf2ls07prem',
      '/collections/etf2ls06prem',
      '/collections/etf2ls05prem'
    ]
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  env: {
    BASE_URL: process.env.BASE_URL || 'http://localhost:3000'
  }
}
