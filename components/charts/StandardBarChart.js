import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: {
    data: {
      type: Object,
      default: null
    }
  },
  mounted() {
    this.renderChart(this.data, {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            display: false,
            stacked: false,
            ticks: {
              beginAtZero: true
            }
          }
        ]
      },
      plugins: {
        deferred: {
          enabled: true,
          delay: 300,
          xOffset: '50%'
        }
      }
    })
  }
}
