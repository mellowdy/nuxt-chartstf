import { Doughnut } from 'vue-chartjs'

export default {
  extends: Doughnut,
  name: 'MedigunChart',
  props: {
    data: {
      type: Object,
      default: null
    }
  },
  mounted() {
    this.renderChart(this.data,
      {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          deferred: {
            enabled: true,
            delay: 300,
            xOffset: '50%'
          }
        }
      })
  }
}
